---
description: TypeScript real life examples.
---

# Env Config Example

```typescript
type BaseURL =
    "baseUrl-1"
  | "baseUrl-2"
  | "baseUrl-3";

type API = "feature-1" | "feature-2";

type ContextRoot = { contextRoot: string };

type Services = "service-1" | "service-2" | "service-3" | "service-4" | "service-5";

export type EnvConfig = {
  keepAliveMsecs: number,
  connector: {
    api: {
      [key in API]: string;
    },
    baseUrl: {
     [key in BaseURL]: string;
    },
    // foo: {[key in Services]: ContextRoot} // This works.
    [key in Services]: ContextRoot; // Does not work.
    // [key in keyof Services]: ServicesContextRoot; // Does not work.
  },
};types
```

The above produces the following error:

> EnvConfig1.ts:21:5 - error TS7061: A mapped type may not declare properties or methods.

because a mapped type can't have other properties but the final type can be an union type of multiple things, one of them being mapped, doesn't really matter you have to see them as independent, one mapped type and another object type, both of them bound together by an union

Mapped types can't have other properties, but the final type can be an union type of multiple things, included being mapped. See:

* [https://www.typescriptlang.org/docs/handbook/2/mapped-types.html](https://www.typescriptlang.org/docs/handbook/2/mapped-types.html)

But we can do this instead:

```typescript
export type EnvConfig = {
  keepAliveMsecs: number,
  connector: {
    api: {
      [key in API]: string;
    },
    baseUrl: {
     [key in BaseURL]: string;
    },
    // foo: {[key in Services]: ContextRoot} // This works.
    // [key in Services]: ContextRoot; // Does not work.
    // [key in keyof Services]: ServicesContextRoot; // Does not work.
  } & {
    [key in Service]: { context: string };
  },
};
```

![](../.gitbook/assets/2021-12-15-07-15-typescript-mapped-types-union.gif)

Note that we do:

```
... connector: { ... } & { [key in Service]: { context: string } }.
```

Therefore, we add more keys to the `connector`.
